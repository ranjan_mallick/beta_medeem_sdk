//
//  AppDelegate.m
//  xyz
//
//  Created by Ranjan on 7/30/13.
//  Copyright (c) 2013 Ranjan. All rights reserved.
//

#import "AppDelegate.h"
//#import "Personn.pb.h"
//#import "Protocol.pb.h"
//#import "MedeemSingleton.h"
#include <iostream>
#include <string>

@implementation AppDelegate

using namespace std;

#define kIpAddress @"a.mcx.io"
#define kPort 6299

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  /*  self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    */
 //   dispatch_queue_t mainQueue = dispatch_get_main_queue();
//	asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:mainQueue];
//	[self normalConnect];

    
   // [self writeCoordinate:touchType point:touchPosition];
    
   // Session *ses = [[Session builder]start:23];
//    Person* person = [[[[[Person builder] setId:123] setName:@"Bob"]setEmail:@"bob@example.com"] build];
//    NSData* data = [person data];
   /* string strw;
    
    char d[256];
    for (int i=219; i<=223; i++) {
        d[i]=(char)i;
        strw+=(char)i;
    }
    string strt(d);
    cout<<endl<<strw;
    */
    //NSString *strr = [NSString stringWithUTF8String:strw.c_str()];
    
/*
    NSString *errorMessage = [NString stringWithCString:strw.c_str()
                                                encoding:[NSString defaultCStringEncoding]];
    NSLog(@"%@",errorMessage);
  */
   // NSData* dataa = [errorMessage dataUsingEncoding:NSUTF8StringEncoding];
    //NSData *dataa = [NSData dataWithContentsOfURL:[NSURL URLWithString:errorMessage]];
    //NSLog(@"%d",[dataa length]);
//    [asyncSocket writeData:data withTimeout:10.0f tag:0];
    
   // NSString* str = [[NSString alloc] initWithData:dataa encoding:NSNonLossyASCIIStringEncoding];

   // NSLog(@"%@",str);
  //  [[MedeemSingleton sharedManager] start:@"bb029289eb08351694fff831794d3bfe5684cd83" withHost:@"https://cloud.count.ly/"]; // newly added line
    
    //Device* device = [[[[[[Device builder] deviceId:udid] DeviceOS :osVersion] DeviceType:DeviceTypePhone] build];
                      
                      
    

    return YES;
}

/*
- (void)normalConnect
{
	NSError *error = nil;
    
	NSString *host = kIpAddress;
    
	
	if (![asyncSocket connectToHost:host onPort:kPort error:&error])
	{
		NSLog(@"Error connecting: %@", error);
	}
}

- (IBAction)resetConnection:(id)sender{
    if ([asyncSocket isConnected]) {
        [asyncSocket disconnect];
    }
    [self normalConnect];
}

-(void) writeCoordinate:(NSString *)type point:(CGPoint)p{
    NSString* str= [NSString stringWithFormat:@":%@,%d,%d:", type, (int)p.x, (int)p.y];
    NSData* data=[str dataUsingEncoding:NSUTF8StringEncoding];
    [asyncSocket writeData:data withTimeout:10.0f tag:0];
    NSLog(str);
    [asyncSocket readDataWithTimeout:10.0f tag:0];
  //  str = [NSString stringWithUTF8String:[data bytes]];
    NSLog(str);
}



#pragma mark - socket

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
	NSLog(@"socket:%p didConnectToHost:%@ port:%hu", sock, host, port);
}


- (void)socketDidSecure:(GCDAsyncSocket *)sock
{
	NSLog(@"socketDidSecure:%p", sock);
}


- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
	NSLog(@"socketDidDisconnect:%p withError: %@", sock, err);
}

//- (void)onSocket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:0.0{
- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag;{
    //    NSLog(@"onSocket:%p didReadData:%i", 0.0);
    //  [sock readDataToData:[GCDAsyncSocket CRLFData] withTimeout:-1 tag:0];
    NSLog([NSString stringWithFormat:@"From server : %d",[data length]]);
    //NSLog([NSString stringWithUTF8String:[data bytes]]);
    
}


*/
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
