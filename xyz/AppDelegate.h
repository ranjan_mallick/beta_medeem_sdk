//
//  AppDelegate.h
//  xyz
//
//  Created by Ranjan on 7/30/13.
//  Copyright (c) 2013 Ranjan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCDAsyncSocket.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    GCDAsyncSocket *asyncSocket;
}

@property (strong, nonatomic) UIWindow *window;

- (void)normalConnect;
-(void) writeCoordinate:(NSString *)type point:(CGPoint)p;
- (IBAction)resetConnection:(id)sender;

@end
