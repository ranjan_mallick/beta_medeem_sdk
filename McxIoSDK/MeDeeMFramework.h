//
//  MeDeeMFramework.h
//  MeDeeMFramework
//
//  Created by Ranjan on 8/9/13.
//  Copyright (c) 2013 Orion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncSocket.h"

@class EventQueue;
@class DeviceInfo;

@interface MeDeeMFramework : NSObject {
	double unsentSessionLength;
	NSTimer *timer;
	double lastTime;
	BOOL isSuspended;
    EventQueue *eventQueue;
    DeviceInfo *deviceinfo;
//    NSString *devinfo;
    GCDAsyncSocket *asyncSocket;
    
    NSFileManager *fileMgr;
    NSArray *paths;
    NSString *docsDry;
    NSString *file;
    //NSMutableString *LogData;//Edited / Commented by Ranjan
    //NSString *sdf;//Edited / Commented by Ranjan
}

@property (nonatomic,strong) NSFileManager *fileMgr;
@property (nonatomic,strong) NSArray *paths;
@property (nonatomic,strong) NSString *docsDry;
@property (nonatomic,strong) NSString *file;
@property (nonatomic,strong) NSMutableString *LogData;

@property (nonatomic,strong) NSString *devinfo,*sdf;
+ (MeDeeMFramework *)sharedInstance;

//- (void)start:(NSString *)appKey;//Edited / Commented by Ranjan

- (void)recordEvent:(NSString *)key count:(int)count;

- (void)recordEvent:(NSString *)key count:(int)count sum:(double)sum;

- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(int)count;

- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(int)count sum:(double)sum;

- (void)normalConnect;
//-(void) writeCoordinate:(NSString *)type point:(CGPoint);
- (IBAction)resetConnection:(id)sender;

@end