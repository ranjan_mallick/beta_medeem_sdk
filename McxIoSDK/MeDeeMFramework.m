
//
//  MeDeeMFramework.m
//  MeDeeMFramework
//
//  Created by Ranjan on 8/9/13.
//  Copyright (c) 2013 Orion. All rights reserved.
//

#ifndef MEDEEM_DEBUG
#define MEDEEM_DEBUG 0
#endif

#ifndef MEDEEM_IGNORE_INVALID_CERTIFICATES
#define MEDEEM_IGNORE_INVALID_CERTIFICATES 1
#endif

#if MEDEEM_DEBUG
#   define MEDEEM_LOG(fmt, ...) NSLog(fmt, ##__VA_ARGS__)
#else
#   define MEDEEM_LOG(...)
#endif

#define MEDEEM_VERSION "1.0"

#import "MeDeeMFramework.h"
//#import "MedeemOpenUDID.h"
#import <UIKit/UIKit.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "MedeemCORE.h"
#import "Protocol.pb.h"
#import "ProtocolBuffers.h"
//#import "Personn.pb.h"
#import "MedeemCORE.h"
//#import "MeDeeMFramework.h"
#include <sys/types.h>
#include <sys/sysctl.h>

#define kIpAddress @"a.mcx.io"
#define kPort 6299

//using namespace std;

/// Utilities for encoding and decoding URL arguments.
/// This code is from the project google-toolbox-for-mac
@interface NSString (GTMNSStringURLArgumentsAdditions)

/// Returns a string that is escaped properly to be a URL argument.
//
/// This differs from stringByAddingPercentEscapesUsingEncoding: in that it
/// will escape all the reserved characters which
/// stringByAddingPercentEscapesUsingEncoding would leave.
///
/// This will also escape '%', so this should not be used on a string that has
/// already been escaped unless double-escaping is the desired result.
- (NSString*)gtm_stringByEscapingForURLArgument;

/// Returns the unescaped version of a URL argument
//
/// This has the same behavior as stringByReplacingPercentEscapesUsingEncoding:,
/// except that it will also convert '+' to space.
- (NSString*)gtm_stringByUnescapingFromURLArgument;

@end

#define GTMNSMakeCollectable(cf) ((id)(cf))
#define GTMCFAutorelease(cf) ([GTMNSMakeCollectable(cf) autorelease])

@implementation NSString (GTMNSStringURLArgumentsAdditions)

- (NSString*)gtm_stringByEscapingForURLArgument {
	// Encode all the reserved characters
	CFStringRef escaped =
    CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                            (CFStringRef)self,
                                            NULL,
                                            (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                            kCFStringEncodingUTF8);
	return GTMCFAutorelease(escaped);
}

- (NSString*)gtm_stringByUnescapingFromURLArgument {
	NSMutableString *resultString = [NSMutableString stringWithString:self];
	[resultString replaceOccurrencesOfString:@"+"
								  withString:@" "
									 options:NSLiteralSearch
									   range:NSMakeRange(0, [resultString length])];
	return [resultString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

/*-(void)getmydata()
 {
 Device* device = [[[[[[Device builder] deviceId:"123321"] OSType:osVersion] DeviceType:DeviceTypePhone] build];
 //Geo* geo = [[[[[[Geo builder] ip:udid]  :osVersion] DeviceType:DeviceTypePhone] build];
 
 
 //Geo* geo = [[[[[[Geo builder] tz:udid]  :osVersion] DeviceType:DeviceTypePhone] build];
 
 
 Session* session = [[[[[Session builder] sessionId:123] Duration:@"Bob"] setEmail:@"bob@example.com"] SessionData:@"Hi.."] build];
 
 
 NSData* data = [person data];
 }
 
 */
@end


@interface DeviceInfo : NSObject
{
}
@end

@implementation DeviceInfo

+ (NSString *)udid
{
    
	return [[[UIDevice currentDevice] identifierForVendor] UUIDString];;
}

+ (NSString *)device
{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}

+ (NSString *)osVersion
{
	return [[UIDevice currentDevice] systemVersion];
}

+ (NSString *)carrier
{
	if (NSClassFromString(@"CTTelephonyNetworkInfo"))
	{
		CTTelephonyNetworkInfo *netinfo = [[[CTTelephonyNetworkInfo alloc] init] autorelease];
		CTCarrier *carrier = [netinfo subscriberCellularProvider];
		//NSLog(@"ca ",[carrier carrierName]);
        return [carrier carrierName];
        
	}
    
	return nil;
}

+ (NSString *)resolution
{
	CGRect bounds = [[UIScreen mainScreen] bounds];
	CGFloat scale = [[UIScreen mainScreen] respondsToSelector:@selector(scale)] ? [[UIScreen mainScreen] scale] : 1.f;
	CGSize res = CGSizeMake(bounds.size.width * scale, bounds.size.height * scale);
	NSString *result = [NSString stringWithFormat:@"%gx%g", res.width, res.height];
    
	return result;
}

+(BOOL)retina
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]
        && [[UIScreen mainScreen] scale] == 2.0) {
        // Retina
        return YES;
    } else {
        // Not Retina
        return NO;
    }
}

+ (NSString *)locale
{
	return [[NSLocale currentLocale] localeIdentifier];
}

+ (NSString *)appVersion
{
    NSString *result = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if ([result length] == 0)
        result = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey];
    
    return result;
}

+ (NSString *)metrics
{
	NSString *result = @"{";
    
	result = [result stringByAppendingFormat:@"\"%@\":\"%@\"", @"_device", [DeviceInfo device]];
    
	result = [result stringByAppendingFormat:@",\"%@\":\"%@\"", @"_os", @"iOS"];
    
	result = [result stringByAppendingFormat:@",\"%@\":\"%@\"", @"_os_version", [DeviceInfo osVersion]];
    
	NSString *carrier = [DeviceInfo carrier];
	if (carrier != nil)
		result = [result stringByAppendingFormat:@",\"%@\":\"%@\"", @"_carrier", carrier];
    
	result = [result stringByAppendingFormat:@",\"%@\":\"%@\"", @"_resolution", [DeviceInfo resolution]];
    
	result = [result stringByAppendingFormat:@",\"%@\":\"%@\"", @"_locale", [DeviceInfo locale]];
    
	result = [result stringByAppendingFormat:@",\"%@\":\"%@\"", @"_app_version", [DeviceInfo appVersion]];
    
	result = [result stringByAppendingString:@"}"];
    
	result = [result gtm_stringByEscapingForURLArgument];
    
	return result;
}

@end

@interface MedeeMEvent : NSObject
{
}

@property (nonatomic, copy) NSString *key;
@property (nonatomic, retain) NSDictionary *segmentation;
@property (nonatomic, assign) int count;
@property (nonatomic, assign) double sum;
@property (nonatomic, assign) double timestamp;

@end

@implementation MedeeMEvent

@synthesize key = key_;
@synthesize segmentation = segmentation_;
@synthesize count = count_;
@synthesize sum = sum_;
@synthesize timestamp = timestamp_;

- (id)init
{
    if (self = [super init])
    {
        key_ = nil;
        segmentation_ = nil;
        count_ = 0;
        sum_ = 0;
        timestamp_ = 0;
    }
    return self;
}

- (void)dealloc
{
    [key_ release];
    [segmentation_ release];
    [super dealloc];
}

@end

@interface EventQueue : NSObject

@end


@implementation EventQueue

- (void)dealloc
{
    [super dealloc];
}

- (NSUInteger)count
{
    @synchronized (self)
    {
        return [[MedeemCORE sharedInstance] getEventCount];
    }
}


- (NSString *)events
{
    NSString *result = @"[";
    
    @synchronized (self)
    {
        NSArray* events = [[[MedeemCORE sharedInstance] getEvents] copy];
        for (NSUInteger i = 0; i < events.count; ++i)
        {
            
            MedeeMEvent *event = [self convertNSManagedObjectToMedeemEvent:[events objectAtIndex:i]];
            
            result = [result stringByAppendingString:@"{"];
            
            result = [result stringByAppendingFormat:@"\"%@\":\"%@\"", @"key", event.key];
            
            if (event.segmentation)
            {
                NSString *segmentation = @"{";
                
                NSArray *keys = [event.segmentation allKeys];
                for (NSUInteger i = 0; i < keys.count; ++i)
                {
                    NSString *key = [keys objectAtIndex:i];
                    NSString *value = [event.segmentation objectForKey:key];
                    
                    segmentation = [segmentation stringByAppendingFormat:@"\"%@\":\"%@\"", key, value];
                    
                    if (i + 1 < keys.count)
                        segmentation = [segmentation stringByAppendingString:@","];
                }
                segmentation = [segmentation stringByAppendingString:@"}"];
                
                result = [result stringByAppendingFormat:@",\"%@\":%@", @"segmentation", segmentation];
            }
            
            result = [result stringByAppendingFormat:@",\"%@\":%d", @"count", event.count];
            
            if (event.sum > 0)
            {
                result = [result stringByAppendingFormat:@",\"%@\":%g", @"sum", event.sum];
            }
            
            result = [result stringByAppendingFormat:@",\"%@\":%ld", @"timestamp", (time_t)event.timestamp];
            
            result = [result stringByAppendingString:@"}"];
            
            if (i + 1 < events.count)
                result = [result stringByAppendingString:@","];
            
            [[MedeemCORE sharedInstance] removeFromQueue:[events objectAtIndex:i]];
            
        }
        
        [events release];
        
    }
    
    result = [result stringByAppendingString:@"]"];
    
    result = [result gtm_stringByEscapingForURLArgument];
    
	return result;
}

-(MedeeMEvent*) convertNSManagedObjectToMedeemEvent:(NSManagedObject*)managedObject{
    MedeeMEvent* event = [[[MedeeMEvent alloc] init] autorelease];//commented by Ranjan
    event.key = [managedObject valueForKey:@"key"];
    if ([managedObject valueForKey:@"count"])
        event.count = ((NSNumber*) [managedObject valueForKey:@"count"]).doubleValue;
    if ([managedObject valueForKey:@"sum"])
        event.sum = ((NSNumber*) [managedObject valueForKey:@"sum"]).doubleValue;
    if ([managedObject valueForKey:@"timestamp"])
        event.timestamp = ((NSNumber*) [managedObject valueForKey:@"timestamp"]).doubleValue;
    if ([managedObject valueForKey:@"segmentation"])
        event.segmentation = [managedObject valueForKey:@"segmentation"];
    return event;
    
}

- (void)recordEvent:(NSString *)key count:(int)count
{
    @synchronized (self)
    {
        NSArray* events = [[MedeemCORE sharedInstance] getEvents];
        for (NSManagedObject* obj in events)
        {
            MedeeMEvent *event = [self convertNSManagedObjectToMedeemEvent:obj];
            if ([event.key isEqualToString:key])
            {
                event.count += count;
                event.timestamp = (event.timestamp + time(NULL)) / 2;
                
                [obj setValue:[NSNumber numberWithDouble:event.count] forKey:@"count"];
                [obj setValue:[NSNumber numberWithDouble:event.timestamp] forKey:@"timestamp"];
                
                [[MedeemCORE sharedInstance] saveContext];
                return;
            }
        }
        
        MedeeMEvent *event = [[MedeeMEvent alloc] init];
        event.key = key;
        event.count = count;
        event.timestamp = time(NULL);
        
        [[MedeemCORE sharedInstance] createEvent:event.key count:event.count sum:event.sum segmentation:event.segmentation timestamp:event.timestamp];
        
        [event release];
    }
}

- (void)recordEvent:(NSString *)key count:(int)count sum:(double)sum
{
    @synchronized (self)
    {
        NSArray* events = [[MedeemCORE sharedInstance] getEvents];
        for (NSManagedObject* obj in events)
        {
            MedeeMEvent *event = [self convertNSManagedObjectToMedeemEvent:obj];
            if ([event.key isEqualToString:key])
            {
                event.count += count;
                event.sum += sum;
                event.timestamp = (event.timestamp + time(NULL)) / 2;
                
                [obj setValue:[NSNumber numberWithDouble:event.count] forKey:@"count"];
                [obj setValue:[NSNumber numberWithDouble:event.sum] forKey:@"sum"];
                [obj setValue:[NSNumber numberWithDouble:event.timestamp] forKey:@"timestamp"];
                
                [[MedeemCORE sharedInstance] saveContext];
                
                return;
            }
        }
        
        MedeeMEvent *event = [[MedeeMEvent alloc] init];
        event.key = key;
        event.count = count;
        event.sum = sum;
        event.timestamp = time(NULL);
        
        [[MedeemCORE sharedInstance] createEvent:event.key count:event.count sum:event.sum segmentation:event.segmentation timestamp:event.timestamp];
        
        [event release];
    }
}

- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(int)count;
{
    @synchronized (self)
    {
        
        NSArray* events = [[MedeemCORE sharedInstance] getEvents];
        for (NSManagedObject* obj in events)
        {
            MedeeMEvent *event = [self convertNSManagedObjectToMedeemEvent:obj];
            if ([event.key isEqualToString:key] &&
                event.segmentation && [event.segmentation isEqualToDictionary:segmentation])
            {
                event.count += count;
                event.timestamp = (event.timestamp + time(NULL)) / 2;
                
                [obj setValue:[NSNumber numberWithDouble:event.count] forKey:@"count"];
                [obj setValue:[NSNumber numberWithDouble:event.timestamp] forKey:@"timestamp"];
                
                [[MedeemCORE sharedInstance] saveContext];
                
                return;
            }
        }
        
        MedeeMEvent *event = [[MedeeMEvent alloc] init];
        event.key = key;
        event.segmentation = segmentation;
        event.count = count;
        event.timestamp = time(NULL);
        
        [[MedeemCORE sharedInstance] createEvent:event.key count:event.count sum:event.sum segmentation:event.segmentation timestamp:event.timestamp];
        
        [event release];
    }
}

- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(int)count sum:(double)sum;
{
    @synchronized (self)
    {
        
        NSArray* events = [[[[MedeemCORE sharedInstance] getEvents] copy] autorelease];
        for (NSManagedObject* obj in events)
        {
            MedeeMEvent *event = [self convertNSManagedObjectToMedeemEvent:obj];
            if ([event.key isEqualToString:key] &&
                event.segmentation && [event.segmentation isEqualToDictionary:segmentation])
            {
                event.count += count;
                event.sum += sum;
                event.timestamp = (event.timestamp + time(NULL)) / 2;
                
                [obj setValue:[NSNumber numberWithDouble:event.count] forKey:@"count"];
                [obj setValue:[NSNumber numberWithDouble:event.sum] forKey:@"sum"];
                [obj setValue:[NSNumber numberWithDouble:event.timestamp] forKey:@"timestamp"];
                
                [[MedeemCORE sharedInstance] saveContext];
                
                return;
            }
        }
        
        MedeeMEvent *event = [[MedeeMEvent alloc] init];
        event.key = key;
        event.segmentation = segmentation;
        event.count = count;
        event.sum = sum;
        event.timestamp = time(NULL);
        
        [[MedeemCORE sharedInstance] createEvent:event.key count:event.count sum:event.sum segmentation:event.segmentation timestamp:event.timestamp];
        
        [event release];
    }
}

@end

@interface ConnectionQueue : NSObject
{
	NSURLConnection *connection_;
	UIBackgroundTaskIdentifier bgTask_;
	NSString *appKey;
	NSString *appHost;
}

@property (nonatomic, copy) NSString *appKey;
@property (nonatomic, copy) NSString *appHost;

@end

static ConnectionQueue *s_sharedConnectionQueue = nil;

@implementation ConnectionQueue : NSObject

@synthesize appKey;
@synthesize appHost;

+ (ConnectionQueue *)sharedInstance
{
	if (s_sharedConnectionQueue == nil)
		s_sharedConnectionQueue = [[ConnectionQueue alloc] init];
    
	return s_sharedConnectionQueue;
}

- (id)init
{
	if (self = [super init])
	{
		connection_ = nil;
        bgTask_ = UIBackgroundTaskInvalid;
        appKey = nil;
        appHost = nil;
	}
	return self;
}

- (void) tick
{
    NSArray* dataQueue = [[[[MedeemCORE sharedInstance] getQueue] copy] autorelease];
    
    if (connection_ != nil || bgTask_ != UIBackgroundTaskInvalid || [dataQueue count] == 0)
        return;
    
    UIApplication *app = [UIApplication sharedApplication];
    bgTask_ = [app beginBackgroundTaskWithExpirationHandler:^{
		[app endBackgroundTask:bgTask_];
		bgTask_ = UIBackgroundTaskInvalid;
    }];
    
    NSString *data = [[dataQueue objectAtIndex:0] valueForKey:@"post"];
    NSString *urlString = [NSString stringWithFormat:@"%@/i?%@", self.appHost, data];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    connection_ = [NSURLConnection connectionWithRequest:request delegate:self];
    
    //[dataQueue release];
}

- (void)beginSession
{
	NSString *data = [NSString stringWithFormat:@"app_key=%@&device_id=%@&timestamp=%ld&sdk_version="MEDEEM_VERSION"&begin_session=1&metrics=%@",
					  appKey,
					  [DeviceInfo udid],
					  time(NULL),
					  [DeviceInfo metrics]];
    
    [[MedeemCORE sharedInstance] addToQueue:data];
    
	[self tick];
}

- (void)updateSessionWithDuration:(int)duration
{
	NSString *data = [NSString stringWithFormat:@"app_key=%@&device_id=%@&timestamp=%ld&session_duration=%d",
					  appKey,
					  [DeviceInfo udid],
					  time(NULL),
					  duration];
    
    [[MedeemCORE sharedInstance] addToQueue:data];
    
	[self tick];
}

- (void)endSessionWithDuration:(int)duration
{
	NSString *data = [NSString stringWithFormat:@"app_key=%@&device_id=%@&timestamp=%ld&end_session=1&session_duration=%d",
					  appKey,
					  [DeviceInfo udid],
					  time(NULL),
					  duration];
    
    [[MedeemCORE sharedInstance] addToQueue:data];
    
	[self tick];
}

- (void)recordEvents:(NSString *)events
{
	NSString *data = [NSString stringWithFormat:@"app_key=%@&device_id=%@&timestamp=%ld&events=%@",
					  appKey,
					  [DeviceInfo udid],
					  time(NULL),
					  events];
    
    [[MedeemCORE sharedInstance] addToQueue:data];
    
	[self tick];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSArray* dataQueue = [[[MedeemCORE sharedInstance] getQueue] copy];
    
	MEDEEM_LOG(@"ok -> %@", [dataQueue objectAtIndex:0]);
    
    UIApplication *app = [UIApplication sharedApplication];
    if (bgTask_ != UIBackgroundTaskInvalid)
    {
        [app endBackgroundTask:bgTask_];
        bgTask_ = UIBackgroundTaskInvalid;
    }
    
    connection_ = nil;
    
    [[MedeemCORE sharedInstance] removeFromQueue:[dataQueue objectAtIndex:0]];
    
    [dataQueue release];
    
    [self tick];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)err
{
#if MEDEEM_DEBUG
    NSArray* dataQueue = [[[MedeemCORE sharedInstance] getQueue] copy];
    MEDEEM_LOG(@"error -> %@: %@", [dataQueue objectAtIndex:0], err);
#endif
    
    UIApplication *app = [UIApplication sharedApplication];
    if (bgTask_ != UIBackgroundTaskInvalid)
    {
        [app endBackgroundTask:bgTask_];
        bgTask_ = UIBackgroundTaskInvalid;
    }
    
    connection_ = nil;
}

#if MEDEEM_IGNORE_INVALID_CERTIFICATES
- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
    [[challenge sender] continueWithoutCredentialForAuthenticationChallenge:challenge];
}
#endif

- (void)dealloc
{
	[super dealloc];
	
	if (connection_)
		[connection_ cancel];
	
	self.appKey = nil;
	self.appHost = nil;
}

@end

static MeDeeMFramework *s_sharedMedeem= nil;

@implementation MeDeeMFramework

@synthesize file,fileMgr,docsDry,paths;


+ (MeDeeMFramework *)sharedInstance
{
	if (s_sharedMedeem == nil)
		s_sharedMedeem = [[MeDeeMFramework alloc] init];
    
	return s_sharedMedeem;
}

- (id)init
{
	if (self = [super init])
	{
		timer = nil;
		isSuspended = NO;
		unsentSessionLength = 0;
        
        Device* device = [[[[[[[[[[[[[Device builder]setDeviceId:[DeviceInfo udid]] setOsVersion:[DeviceInfo osVersion]]setCarrier:[DeviceInfo carrier]]setRetina:[DeviceInfo retina]]setType:DeviceTypePhone]setOs:DeviceOSIos]setDensity:6]setH:6]setMake:@"APPLE"]setModel:@"IPHONE"]setW:9] build];
        
        Data_Builder* dbdr = [[Data_Builder alloc]init];
        
        [dbdr setDevice:device];
        
        App_Builder *appbdr = [[App_Builder alloc]init];
        [appbdr setAppKey:@"XDzSB4A3RE71EkNTrQM0s8DXOZPWAPCa"];
        App *apps = [appbdr build];//commented by Ranjan
        [dbdr setApp:apps];
        
        User_Builder *usrbdr = [[User_Builder alloc]init];//commented by Ranjan
        [usrbdr addDeviceId:[DeviceInfo udid]];
        User *usr = [usrbdr build];
        [dbdr setUser:usr];
        
        Data *dats = [[dbdr build] copy];//Edited / Commented by Ranjan
        
        //NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];////commented by Ranjan
        
        // NSTimeInterval is defined as double
        //NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];//commented by Ranjan
        
        
        Init *init = [[[[Init builder]setInitData:dats]setTimestamp:12345] build];//commented by Ranjan
        
        NSLog(@"%lld",init.timestamp);
        
        Request_Builder *reqbdr = [[Request_Builder alloc]init];
        [reqbdr setTimestamp:12345];//commented by Ranjan
        
        [reqbdr setInitData:dats];
        [reqbdr setReqType:Request_ReqTypeInit];
        Request *req = [reqbdr build];
        NSData *devdata = [req data];
        
        
        //NSData *devdata = [device data];
        
        /*
         //        Device* device = [[[Device builder]setDeviceId:[DeviceInfo udid]] setCarrier:[DeviceInfo carrier]];
         
         //   [device setCarrier:[DeviceInfo carrier]];
         //  [device setRetina:[DeviceInfo retina]];
         // [device setMake:@"My Make"];
         //[device setModel:@"My Model"];
         
         Data *dat = [[[Data builder] setDevice:device]build];
         App *app = [[[App builder] setAppKey:@"appkeY"]build];
         [dat setApp:app];
         
         User *user = [[[User builder] addDeviceId:[DeviceInfo udid]]build];
         [dat setUser:user];
         
         
         Init *init = [[[[Init builder]setInitData:dat]setTimestamp:12345] build];
         NSData* ddd = [init data];
         Person* person = [[[[[Person builder] setId:123] setName:@"Bob"]setEmail:@"bob@example.com"] build];
         //  NSData *dd = [Person data];
         NSData* datt = [person data];
         
         //NSData *dataSocket = [dat data];
         
         */
        //self.LogData = [[NSMutableString alloc]init];
        //self.sdf=@"ding dong";
        // NSLog(@"Framework Can Access");//Edited/Commented bY Ranjan
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:mainQueue];
        
        fileMgr = [NSFileManager defaultManager];
        paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDry = [paths objectAtIndex:0];
        file = [docsDry stringByAppendingPathComponent:@"FWLog.txt"];
        if ([fileMgr fileExistsAtPath:file]) {
            //NSLog(@"Can Read The File");////Edited/Commented bY Ranjan
        }
        else
        {
            [fileMgr createFileAtPath:file contents:nil attributes:nil];
            
        }
        [self.LogData appendString:@"++ About to call normalConnect ++"];
        [self.LogData writeToFile:file atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
        [self normalConnect];
        //NSLog(@"Framework Can Connect");//Edited/Commented bY Ranjan
        [self.LogData appendString:@"++ About to write data to Server"];
        [self.LogData writeToFile:file atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
        [asyncSocket writeData:devdata withTimeout:10.0f tag:0];
        
        //NSLog(@"SYNC FRAMEWORK DATA");//Edited/Commented bY Ranjan
        
        
        
        //[device :[DeviceInfo metrics]];
        // Not Retina
        
        //    [device set]
        
        //        NSLog(@"%@",device.carrier);
        // [device se[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[tMake:[DeviceInfo ]]
        //  NSLog(@"%@",[[UIDevice currentDevice] plat])
        
        
        /*
         device.setMake(android.os.Build.MANUFACTURER);
         device.setModel(android.os.Build.MODEL);
         
         device.setRetina(false);
         setDisplayMetrics(device);
         setDeviceType(device);
         
         data = Data.newBuilder();
         data.setDevice(device);
         
         //Now on to the app
         App.Builder app = App.newBuilder();
         app.setAppKey(appKey);
         //rest of the app data will be populated FROM the server.init call
         data.setApp(app);
         
         
         //User setup - more on this to be bround from Medeem.java
         user = User.newBuilder();
         user.addDeviceId(device.getDeviceId());
         data.setUser(user);
         
         initServer();
         
         //initServer's response would've updated our data with relevant values. Lets store the bugger in prefs.
         write();
         //		String dataStr = data.build().toByteString().toStringUtf8();
         prefs.edit().putBoolean(INITED, true).apply();
         */
        eventQueue = [[EventQueue alloc] init];
        self.devinfo = [DeviceInfo device];
        //self.sdf=@"DeviceInfo";//commented by Ranjan
        
        //UIDevice *d;
        //  Device* dev = [[[Device builder]setDeviceId:[DeviceInfo udid]] setOsVersion:[DeviceInfo osVersion]] ;
        //Device* dev = [Device builder];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(didEnterBackgroundCallBack:)
													 name:UIApplicationDidEnterBackgroundNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(willEnterForegroundCallBack:)
													 name:UIApplicationWillEnterForegroundNotification
												   object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(willTerminateCallBack:)
													 name:UIApplicationWillTerminateNotification
												   object:nil];
        [self.LogData writeToFile:file atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
        [reqbdr release];//Eited / Commented By Ranjan
        [appbdr release];
        [usrbdr release];
        [dbdr release];
        [dats release];
        
	}
    
	return self;
}

- (void)start:(NSString *)appKey withHost:(NSString *)appHost
{
	timer = [NSTimer scheduledTimerWithTimeInterval:60.0
											 target:self
										   selector:@selector(onTimer:)
										   userInfo:nil
											repeats:YES];
	lastTime = CFAbsoluteTimeGetCurrent();
	[[ConnectionQueue sharedInstance] setAppKey:appKey];
	[[ConnectionQueue sharedInstance] setAppHost:appHost];
	[[ConnectionQueue sharedInstance] beginSession];
}

- (void)recordEvent:(NSString *)key count:(int)count
{
    [eventQueue recordEvent:key count:count];
    
    if (eventQueue.count >= 10)
        [[ConnectionQueue sharedInstance] recordEvents:[eventQueue events]];
}

- (void)recordEvent:(NSString *)key count:(int)count sum:(double)sum
{
    [eventQueue recordEvent:key count:count sum:sum];
    
    if (eventQueue.count >= 10)
        [[ConnectionQueue sharedInstance] recordEvents:[eventQueue events]];
}

- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(int)count
{
    [eventQueue recordEvent:key segmentation:segmentation count:count];
    
    if (eventQueue.count >= 10)
        [[ConnectionQueue sharedInstance] recordEvents:[eventQueue events]];
}

- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(int)count sum:(double)sum
{
    [eventQueue recordEvent:key segmentation:segmentation count:count sum:sum];
    
    if (eventQueue.count >= 10)
        [[ConnectionQueue sharedInstance] recordEvents:[eventQueue events]];
}

- (void)onTimer:(NSTimer *)timer
{
	if (isSuspended == YES)
		return;
    
	double currTime = CFAbsoluteTimeGetCurrent();
	unsentSessionLength += currTime - lastTime;
	lastTime = currTime;
    
	int duration = unsentSessionLength;
	[[ConnectionQueue sharedInstance] updateSessionWithDuration:duration];
	unsentSessionLength -= duration;
    
    if (eventQueue.count > 0)
        [[ConnectionQueue sharedInstance] recordEvents:[eventQueue events]];
}

- (void)suspend
{
	isSuspended = YES;
    
    if (eventQueue.count > 0)
        [[ConnectionQueue sharedInstance] recordEvents:[eventQueue events]];
    
	double currTime = CFAbsoluteTimeGetCurrent();
	unsentSessionLength += currTime - lastTime;
    
	int duration = unsentSessionLength;
	[[ConnectionQueue sharedInstance] endSessionWithDuration:duration];
	unsentSessionLength -= duration;
}

- (void)resume
{
	lastTime = CFAbsoluteTimeGetCurrent();
    
	[[ConnectionQueue sharedInstance] beginSession];
    
	isSuspended = NO;
}

- (void)exit
{
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
	
	if (timer)
    {
        [timer invalidate];
        timer = nil;
    }
    
    [eventQueue release];
    
	
	[super dealloc];
}

- (void)didEnterBackgroundCallBack:(NSNotification *)notification
{
    
    /*  Person* person = [[[[[Person builder] setId:123]
     setName:@"Bob"]
     setEmail:@"bob@example.com"] build];
     NSData* data = [person data];
     */
    
    
    
    
	//MEDEEM_LOG(@"Medeem didEnterBackgroundCallBack");
	[self suspend];
    
}

- (void)willEnterForegroundCallBack:(NSNotification *)notification
{
	//MEDEEM_LOG(@"Medeem willEnterForegroundCallBack");
	[self resume];
}

- (void)willTerminateCallBack:(NSNotification *)notification
{
	//MEDEEM_LOG(@"Medeem willTerminateCallBack");
    //  [[MedeemCORE sharedInstance] saveContext];
	[self exit];
}

- (void)normalConnect
{
	NSError *error = nil;
    
	NSString *host = kIpAddress;
    
	
	if (![asyncSocket connectToHost:host onPort:kPort error:&error])
	{
        [self.LogData appendString:[NSString stringWithFormat:@"Error connecting: %@", error]];
        [self.LogData writeToFile:file atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
		//NSLog(@"Error connecting: %@", error);//Edited/Commented bY Ranjan
	}
}

- (IBAction)resetConnection:(id)sender{
    if ([asyncSocket isConnected]) {
        [asyncSocket disconnect];
    }
    [self normalConnect];
}

-(void) writeCoordinate:(NSString *)type point:(CGPoint)p{
    NSString* str= [NSString stringWithFormat:@":%@,%d,%d:", type, (int)p.x, (int)p.y];
    NSData* data=[str dataUsingEncoding:NSUTF8StringEncoding];
    [asyncSocket writeData:data withTimeout:10.0f tag:0];
    //NSLog(str);//Edited/Commented bY Ranjan
    [asyncSocket readDataWithTimeout:10.0f tag:0];
    //  str = [NSString stringWithUTF8String:[data bytes]];
    // NSLog(str);////Edited/Commented bY Ranjan
}



#pragma mark - socket

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
	//NSLog(@"socket:%p didConnectToHost:%@ port:%hu", sock, host, port);////Edited/Commented bY Ranjan
    [self.LogData appendString:[NSString stringWithFormat:@"socket:%p didConnectToHost:%@ port:%hu", sock, host, port]];
    [self.LogData writeToFile:file atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
}


- (void)socketDidSecure:(GCDAsyncSocket *)sock
{
    [self.LogData appendString:[NSString stringWithFormat:@"socketDidSecure:%p", sock]];
    [self.LogData writeToFile:file atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
	//NSLog(@"socketDidSecure:%p", sock);////Edited/Commented bY Ranjan
}


- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    [self.LogData appendString:[NSString stringWithFormat:@"socketDidDisconnect:%p withError: %@", sock, err]];
    [self.LogData writeToFile:file atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
	//NSLog(@"socketDidDisconnect:%p withError: %@", sock, err);//Edited/Commented bY Ranjan
}

//- (void)onSocket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:0.0{
- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag;{
    //    NSLog(@"onSocket:%p didReadData:%i", 0.0);
    //  [sock readDataToData:[GCDAsyncSocket CRLFData] withTimeout:-1 tag:0];
    
    [self.LogData appendString:[NSString stringWithFormat:@"From server : %d",[data length]]];
    [self.LogData writeToFile:file atomically:NO encoding:NSStringEncodingConversionAllowLossy error:nil];
    //NSLog([NSString stringWithFormat:@"From server : %d",[data length]]); //commented by Ranjan
    
    //NSLog([NSString stringWithUTF8String:[data bytes]]);
    
}

@end
